package com.xfinity.poker;

import java.util.ArrayList;
import java.util.List;

public class Player implements PlayerRules {
	
	private String name;
	private PlayerHand playerHand;
	private double playerChips;
	private int order;
	private boolean isDealer;
	
	private boolean allIn;	
	
	
	public Player(String name,int order){
		this.name = name;
		playerHand = new PlayerHand();
		allIn = false;
		playerChips = PLAYER_INITIAL_CHIPS;
		isDealer = false;
		this.order = order;
	}
	/*
	private List<Chip> createChipsFromMoney(int playerMoney) {
		List<Chip> chips = new ArrayList<Chip>();		
		return chips;
	} */

	public void giveCard(Card card){
		playerHand.add(card);
	}
	
	public int getHandSize(){
		return playerHand.size();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public PlayerHand getPlayerHand() {
		return playerHand;
	}

	public boolean isAllIn() {
		return allIn;
	}

	public void setAllIn(boolean allIn) {
		this.allIn = allIn;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public boolean isDealer() {
		return isDealer;
	}

	public void setDealer(boolean isDealer) {
		this.isDealer = isDealer;
	}
	
	public double getPlayerChips(){
		return playerChips;
	}

	public double takeChips(double amount) {
		if(playerChips < amount){
			//TODO throw exception
			return -1;
		}else{
			playerChips -= amount;
			return amount;
		}
			
	}
	
}
