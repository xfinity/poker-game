package com.xfinity.poker;

public interface GameRules {
	public static final int GAME_PLAYERS = 5;	
	public static final int BIG_BLIND = 20;
	public static final int SMALL_BLIND = 10;
}
