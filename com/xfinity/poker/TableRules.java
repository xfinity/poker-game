package com.xfinity.poker;

public interface TableRules {
	public static final int INITIAL_SMALL_BLIND = 10;
	public static final int INITIAL_BIG_BLIND = 20;
}
