import java.util.ArrayList;
import java.util.List;

import com.xfinity.poker.*;


public class GameTesting implements GameRules {
	
	Player singlePlayer;
	List<Player> computerPlayers;
	
	Dealer dealer;
	Table table;
	
	public GameTesting(){
		createPlayers();
		createTable();
		createDealer();
		startPreFlopRound();
		
	}

	private void startPreFlopRound() {
		
	}

	private void createTable() {
		List<Player> allPlayers = new ArrayList<Player>();
		allPlayers.add(singlePlayer);
		allPlayers.addAll(computerPlayers);
		table = new Table(allPlayers);		
	}

	private void createDealer() {
		dealer = new Dealer();
		dealer.setTable(table);		
	}

	private void createPlayers() {		
		singlePlayer = new Player("Single Player",0);
		singlePlayer.setDealer(true);
		
		computerPlayers = new ArrayList<Player>();
		for(int i=1;i<GAME_PLAYERS;i++)
			computerPlayers.add(new Player("CP"+i,i));
	}
	
	public void gameRun(){
		dealer.collectBlinds();
		dealer.dealToPlayers();
	}

}
